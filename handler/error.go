package handler

import (
	"encoding/json"
	"net/http"
)

// SendError throw error in based on input parameters
func SendError(w http.ResponseWriter, r *http.Request, message string, status int) {
	msg := ErrorMsg{Error: message}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(msg)
}
