package handler

import (
	"log"
	"net/http"
)

// Index is just a type
type Index struct{}

func (hh *Index) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request %s from %s", r.URL.Path, r.RemoteAddr)
	// Check request method is right
	if r.Method != "GET" {
		http.Redirect(w, r, "/v1/_help", http.StatusMethodNotAllowed)
		return
	}
	// Custom 404 error page, in case we didn't hit right route
	if r.URL.Path != "/" {
		errorHandler(w, r, http.StatusNotFound)
		return
	}
	http.Redirect(w, r, "/v1/_help", http.StatusFound)
}
