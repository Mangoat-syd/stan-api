package handler

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// Upload type
type Upload struct{}

// ErrorMsg message struc
type ErrorMsg struct {
	Error string `json:"error"`
}

// Catalog structure to parse POST data
type Catalog struct {
	Payload []struct {
		Country      string `json:"country"`
		Description  string `json:"description"`
		Drm          bool   `json:"drm"`
		EpisodeCount int    `json:"episodeCount"`
		Genre        string `json:"genre"`
		Image        struct {
			ShowImage string `json:"showImage"`
		} `json:"image"`
		Language      string      `json:"language"`
		NextEpisode   interface{} `json:"nextEpisode"`
		PrimaryColour string      `json:"primaryColour"`
		Seasons       []struct {
			Slug string `json:"slug"`
		} `json:"seasons"`
		Slug      string `json:"slug"`
		Title     string `json:"title"`
		TvChannel string `json:"tvChannel"`
	} `json:"payload"`
	Skip         int `json:"skip"`
	Take         int `json:"take"`
	TotalRecords int `json:"totalRecords"`
}

//Upload function
func (rh *Upload) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request %s from %s", r.URL.Path, r.RemoteAddr)
	// Validate method
	if r.Method != "POST" {
		http.Redirect(w, r, "/v1/_help", http.StatusMethodNotAllowed)
		return
	}

	var parsed Catalog

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Panic(err)
		return
	}

	if err := r.Body.Close(); err != nil {
		log.Panic(err)
		return
	}

	if err := json.Unmarshal(body, &parsed); err != nil {
		// Error happen, sending back error message
		SendError(w, r, "Could not decode request: JSON parsing failed", http.StatusBadRequest)
		//		msg := ErrorMsg{Error: "Could not decode request: JSON parsing failed"}
		//		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//		w.WriteHeader(http.StatusBadRequest)
		//		json.NewEncoder(w).Encode(msg)
		return
	}

	arr := parsed.Payload
	resp := new(ResponseMsg)
	for _, v := range arr {
		// Check task conditions +DRM and >0 episodes
		if v.Drm && v.EpisodeCount > 0 {
			log.Printf("%s match conditions", v.Title)
			var e Entry
			e.Title = v.Title
			e.Image = v.Image.ShowImage
			e.Slug = v.Country
			resp.Response = append(resp.Response, e)
		}
	}

	// Send response if nothig was found
	if len(resp.Response) == 0 {
		log.Println("No any matches in here")
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	json.NewEncoder(w).Encode(resp)
	return
}
