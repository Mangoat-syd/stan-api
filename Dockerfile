FROM golang
RUN mkdir /app\
    && git clone https://Mangoat-syd@bitbucket.org/Mangoat-syd/stan-api.git /app\
    && go get bitbucket.org/Mangoat-syd/stan-api/handler
WORKDIR /app
RUN go build main.go
ENTRYPOINT [ "/app/main" ]
EXPOSE 8088 