package handler

import (
	"html/template"
	"log"
	"net/http"
)

var tpls = []string{
	"template/layout.html",
	"template/help.html",
}
var tpl = template.Must(template.ParseFiles(tpls...))

// Help descr
type Help struct{}

func (rh *Help) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request %s from %s", r.URL.Path, r.RemoteAddr)
	if r.Method != "GET" {
		http.Redirect(w, r, "/v1/_help", http.StatusMethodNotAllowed)
		return
	}
	defer tpl.Execute(w, rh)
}
