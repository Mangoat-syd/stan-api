package handler

// ResponseMsg response container
type ResponseMsg struct {
	Response []struct {
		Image string `json:"image"`
		Slug  string `json:"slug"`
		Title string `json:"title"`
	} `json:"response"`
}

// Entry for response
type Entry struct {
	Image string `json:"image"`
	Slug  string `json:"slug"`
	Title string `json:"title"`
}

// EmptyResponse for non matched
type EmptyResponse struct {
	Response string `json:"response"`
}

// Payload entry
type Payload struct {
	Country      string `json:"country"`
	Description  string `json:"description"`
	Drm          bool   `json:"drm"`
	EpisodeCount int    `json:"episodeCount"`
	Genre        string `json:"genre"`
	Image        struct {
		ShowImage string `json:"showImage"`
	} `json:"image"`
	Language      string      `json:"language"`
	NextEpisode   interface{} `json:"nextEpisode"`
	PrimaryColour string      `json:"primaryColour"`
	Seasons       []struct {
		Slug string `json:"slug"`
	} `json:"seasons"`
	Slug      string `json:"slug"`
	Title     string `json:"title"`
	TvChannel string `json:"tvChannel"`
}
