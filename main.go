package main

import (
	"log"
	"net/http"

	"bitbucket.org/Mangoat-syd/stan-api/handler"
)

func main() {
	http.Handle("/", new(handler.Index))
	http.Handle("/v1/_help", new(handler.Help))
	http.Handle("/v1/upload", new(handler.Upload))
	log.Fatal(http.ListenAndServe(":8088", nil))
}
